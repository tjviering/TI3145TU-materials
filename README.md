# Materials of TI3145TU Machine Learning and Introduction to AI (5 EC)

This course teaches the basics of machine learning, including theory and practical aspects, and an introduction to artificial intelligence (AI).

## Learning Objectives 

- LO1 Discuss about the history of AI, AI definitions, AI approaches, and philosophical arguments regarding AI
- LO2 Apply reflex-, search- and logical-based AI techniques to solve puzzles and games with pen and paper.
- LO3 Apply common operations (pre-processing, plotting, etc.) to datasets using Python.
- LO4 Apply the statistical concepts of supervised and unsupervised learning, and recognize their limitations.
- LO5 Analyze which factors impact the performance of learning algorithms.
- LO6 Build and optimize a machine learning pipeline using Python and Scikit-learn and evaluate its performance.
- LO7 Calculate and discuss fairness of machine learning models.

## Prequisite Knowledge for this course

Some basic statistics and basic linear algebra knowledge is necessary. For more details see https://studiegids.tudelft.nl/a101_displayCourse.do?course_id=66128 

## Materials

| Name                                  | Prerequisite knowledge | Subjects                                                | Videos    | Quizzes   | Exercises  |
| ------------------------------------- | ---------------------- | ------------------------------------------------------- | --------- | --------- | ---------- |
| [Introduction to ML](introduction.md) | -                      |  AI                                                     | 3 (30 min) | 2 forum questions   | X Lab exercises | 
| [Training models](training.md)        | linear regression, KNN (high level) | training, stochastic gradient descent, logistic regression | 8 (50 min) | 4 MCQ | X Lab exercises |
| [SVMs](svm.md)                        | classification              | SVM, hard margin, soft margin, kernels                   | 4      | 4 MCQ | X Lab exercises | 
