# Overview Training Models: Gradient Descent and Logistic Regression

After this section students can:

- Explain the basics of iterative training and gradient descent
- Explain the three different variants of gradient descent and their trade-offs
- Tune the parameters of a gradient descent optimizer
- Explain why feature scaling is important for gradient descent
- Explain why logistic regression is better suited to classification than linear regression

## Content

| Video                                                                    | Prerequisite knowledge | Extra material         | Subjects                                                                 |
| ------------------------------------------------------------------------ | ---------------------- | ---------------------- | ------------------------------------------------------------------------ |
| [Basics of Gradient Descent Pt 1](https://youtu.be/ElAA2pPP8ZA) (5:25)         | -                      | Widget 1 + 2 (answers) | Different types of training (closed form, instance based, iterative). Three ingredients for iterative training: cost function, model class, optimization procedure.                                    |
| [Basics of Gradient Descent Pt 2](https://youtu.be/NXj3jnJQ1yE) (2:48)         | -                      | Widget 3 + 4 (answers) | Cost function in 3D, level lines, gradient.                                   |
| [Basics of Gradient Descent Pt 3](https://youtu.be/EbewzR1gq8Q) (2:16)         | -                      | Widget 5 (answers)     | Gradient descent update rule, learning rate, epoch, number of epochs.                                   |
| [Basics of Gradient Descent Pt 4](https://youtu.be/FsjvjrtaDjU) (5:00)          | -                      | Quiz 1                 | When does gradient descent (not) work? Local minima, plateau. Summary of all the parts.                                    |
| [Three Variants of Gradient Descent](https://youtu.be/zwb-WsbImbU) (6:09)       | -                      | -                      | Batch, Stochastic, Mini-batch Gradient Descent, Comparison (memory, speed).     |
| [How to use Gradient Descent in Practice?](https://youtu.be/w37tixmxKxQ) (11:11) | -                      | -                      | How to set the batchsize, learning rate, number of epochs, feature scaling.                                   |
| [Why Do We Need Logistic Regression?](https://youtu.be/yvw_OMroG8o) (5:33)     | -                      | -                      | We cannot optimize error rate with SGD and why linear regression is not ideal for classification. Posterior probability.                                 |
| [Basics of Logistic Regression](https://youtu.be/ll2niouRP-Y) (14:41)                                          | -                      | -                      | Logistic function, likelihood, Negative Log Likelihood (NLL), Log loss, Threshold for classification.  |

## Extra video materials

### Quiz 1

#### Question 1

For gradient descent, the loss function should be smooth and differentiable.

- [ ] False
- [x] True

#### Question 2

The gradient points in the direction where the loss function decreases the most.

- [x] False
- [ ] True

### Quiz 2

#### Question 1

A larger value of C results in a larger margin.

- [x] False
- [ ] True

#### Question 2

A soft-margin SVM can be useful in case of outliers.

- [ ] False
- [x] True

## Project

[Training Models Notebook
](training.ipynb)

## Literature

1. (HO) Hands-On Machine Learning with Scikit-Learn, Keras, and TensorFlow, 2nd Edition. Author: Aurélien Géron. (sorry, this book is not available freely online...)
2. (ISLR) An Introduction to Statistical Learning: With Applications in R (first edition). Authors: Gareth James, Daniela Witten, Trevor Hastie, Robert Tibshirani. [Download PDF freely](https://hastie.su.domains/ISLR2/ISLRv2_corrected_June_2023.pdf.download.html).

- HO p. 111-114 until "The Normal Equation"
- HO p. 118-134 until "Regularized Linear Models"
- HO p. 142 from "Logistic Regression" until p. 148
- ISLR p. 129-130
