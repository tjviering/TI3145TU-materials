# Overview SVMs

After this section students can:

- Explain the concept of margin, support vectors, support vector machine
- Recognize when feature scaling leads to suboptimal SVM classifiers and explain why
- Can give two examples that motivate a soft-margin SVM formulation
- Can compute the hinge loss
- Can explain the influence of the parameter C on soft-margin SVM training

## Content

| Video                                                          | Prerequisite knowledge | Extra material     | Subjects             |
| -------------------------------------------------------------- | ---------------------- | ------------------ | -------------------- |
| [Basics of Hard-Margin SVM Pt 1](https://youtu.be/S_LoC56-Des) | -                      | Widget 1 (answers) | Machine learning, AI |
| [Basics of Hard-Margin SVM Pt 2](https://youtu.be/wScLTgXIONE) | Hard margin Pt 1       | Quiz 1             | Machine learning, AI |
| [Soft margin SVM](https://youtu.be/GGzPrj8bFNs)                | SVM                    | Widget 2(answers)  | Machine learning, AI |
| [Kernels](https://youtu.be/E_-oxhLrZEc)                        | SVM                    | Quiz 2-            | Machine learning, AI |

## Extra video materials

### Quiz 1

#### Question 1

A large margin is useful so that points don’t accidentally cross the boundary

- [ ] False
- [x] True

#### Question 2

Feature scaling is not necessary for SVM’s

- [x] False
- [ ] True

### Quiz 2

#### Question 1

A larger value of C results in a larger margin.

- [x] False
- [ ] True

#### Question 2

A soft-margin SVM can be useful in case of outliers.

- [ ] False
- [x] True

## Project

[Support Vector Machines Notebook
](svm.ipynb)

## Literature

1. (HO) Hands-On Machine Learning with Scikit-Learn, Keras, and TensorFlow, 2nd Edition. Author: Aurélien Géron. (sorry, this book is not available freely online...)
2. (ISLR) An Introduction to Statistical Learning: With Applications in R (first edition). Authors: Gareth James, Daniela Witten, Trevor Hastie, Robert Tibshirani. [Download PDF freely](https://hastie.su.domains/ISLR2/ISLRv2_corrected_June_2023.pdf.download.html).

- HO p. 153-162 until “SVM Regression”
- HO p. 164 from “Under the Hood” until and including Figure 5-13.
- HO p. 172 from “Online SVMs” until p. 174 “Exercises”.
- ISLR p. 337-353
