# Overview Introduction to Machine Learning

After this section students can:

- Explain the basic concept of machine learning and its terminology
- Explain the pros and cons of machine learning versus regular programming
- Identify different types of machine learning settings

## Content

| Video                                                                                    | Prerequisite knowledge | Extra material | Subjects             |
| ---------------------------------------------------------------------------------------- | ---------------------- | -------------- | -------------------- |
| [Why use machine learning](https://youtu.be/1N1yl_Qdz5M)                                 | -                      | -              | Machine learning, AI |
| [Basics & Terminology](https://youtu.be/BjHKOUwqjvY)                                     | -                      | -              | Machine learning, AI |
| [The Biggest Challenge with Machine Learning & Frameworks](https://youtu.be/tlyivMV5POU) | -                      | -              | Machine learning, AI |

## Extra video materials

### Quiz

Give two advantages and two disadvantages of machine learning versus traditional programming.
Choose two applications below and argue whether you think it is suitable to apply machine learning for them:

- Application for booking cinema tickets in real time.
- Application for suggesting most suitable movie for a customer.
- Application for forecasting the weather of the incoming week.
- Application for telling how is the weather at the moment.
- Application for remotely operate a robotic arm.

Choose two applications below and argue whether they are supervised, unsupervised, semi-supervised or reinforcement learning?

- Given a dataset containing pictures from old manuscripts from which some of them we know its original source and some others we do not know, we want to develop an application that can tell us its original - Given a set of labeled images from different objects, we want to develop an application that can recognize specific objects in an image.
- We want to develop an application that can learn to play the original Super Mario by itself.
- Given the database of only data about purchases history from a retail store, we want to develop an application that analyzes customers and groups them by their interests.

## Literature

1. (ISLR) An Introduction to Statistical Learning: With Applications in R (first edition). Authors: Gareth James, Daniela Witten, Trevor Hastie, Robert Tibshirani. [Download PDF freely](https://hastie.su.domains/ISLR2/ISLRv2_corrected_June_2023.pdf.download.html).

- ISLR p. 15-29 until 2.2.
